//
//  NSMutableDictionary+NotNull.m
//  JCProject
//
//  Created by hennychen on 12/7/16.
//  Copyright © 2016 worm_kc. All rights reserved.
//

#import "NSMutableDictionary+NotNull.h"

@implementation NSMutableDictionary (NotNull)
-(void)setNoNullObject:(id)anObject forKey:(id<NSCopying>)aKey
{
    if (anObject) {
        [self setObject:anObject forKey:aKey];
    }
}

-(void)setObject:(id)anObject forKey:(id<NSCopying>)aKey withDefault:(id)defaultValue
{
    if (anObject) {
        [self setObject:anObject forKey:aKey];
    }else if(defaultValue)
    {
        [self setObject:defaultValue forKey:aKey];
    }else
    {
        [self setObject:@"" forKey:aKey];
    }
}

@end
