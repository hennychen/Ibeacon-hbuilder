//
//  PGPluginBLEPrinter.h
//  HBuilder-Hello
//
//  Created by hennychen on 8/3/17.
//  Copyright © 2017 DCloud. All rights reserved.
//

#import <UIKit/UIKit.h>
#include "PGPlugin.h"
#include "PGMethod.h"

@import SeekcyBeaconSDK;

@interface PGPluginBLEPrinter : PGPlugin<SKYBeaconManagerScanDelegate>{
    NSMutableArray *scanUUIDArray;
    NSMutableArray *peripheralsForScanArray;
}

- (void)searchBluetooth:(PGMethod*)commands;
- (void)closeBluetooth:(PGMethod*)commands;//关闭蓝牙
@end
