//
//  NSMutableDictionary+NotNull.h
//  JCProject
//
//  Created by hennychen on 12/7/16.
//  Copyright © 2016 worm_kc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableDictionary (NotNull)
/**
 *  设置非空字典
 *
 *  @param anObject <#anObject description#>
 *  @param aKey     <#aKey description#>
 */
-(void)setNoNullObject:(id)anObject forKey:(id<NSCopying>)aKey;
-(void)setObject:(id)anObject forKey:(id<NSCopying>)aKey withDefault:(id)defaultValue;

@end
