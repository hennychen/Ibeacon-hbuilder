document.addEventListener( "plusready",  function()
                          {
                          var _BARCODE = 'bluetooth',
                          B = window.plus.bridge;
                          var bluetooth =
                          {
                          
                          search : function (Argus1,successCallback,errorCallback)
                          {
                          var success = typeof successCallback !== 'function' ? null : function(args)
                          {
                          successCallback(args);
                          },
                          fail = typeof errorCallback !== 'function' ? null : function(code)
                          {
                          errorCallback(code);
                          };
                          callbackID = B.callbackId(success, fail);
                          return B.exec(_BARCODE, "searchBluetooth", [callbackID,Argus1]);
                          },
                          close : function (Argus1)
                          {
                          return B.exec(_BARCODE, "closeBluetooth", [Argus1]);
                          },
                          cancelBLE : function (Argus1)
                          {
                          return B.exec(_BARCODE, "cancelBluetooth", [Argus1]);
                          },
                          bound : function(Argus1,Argus2){
                          return B.exec(_BARCODE, "boundBluetooth", [Argus1,Argus2]);
                          },
                          pause : function(Argus1){
                          return B.exec(_BARCODE, "pauseBluetooth", [Argus1]);
                          },
                          
                          
                          
                          };
                          window.plus.bluetooth = bluetooth;
                          }, true );
